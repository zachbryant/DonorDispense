package donordispense;

import java.util.ArrayList;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class DispenseListener implements Listener{
    private final DonorDispense main;
    
    public DispenseListener(DonorDispense m){main=m;}
    
    /*
    * Event handler for player interaction events, such as activating switches, buttons, tripwires, etc.
    * @param PlayerInteractEvent e contains crucial information about the event.
    * If the event isnt cancelled, player stands on block or right clicks it,
    *    and the block can either be able to: 
    *        1. be powered, and is next to a possible power source. 
    *        2. generate power. 
    *        3. carry power (e.g. wire)
    */
    @EventHandler(priority=EventPriority.HIGHEST)
    public void onPlayerInteract(PlayerInteractEvent e){
        Block b= e.getClickedBlock();
        if(b==null)
            return;
        boolean trigger =main.utils.isTrigger(b);
        boolean action =e.getAction()==Action.RIGHT_CLICK_BLOCK || e.getAction()==Action.PHYSICAL;
        //check if the block is a dispenser, because things wont work if it is
        if(!e.isCancelled() && b.getType()!=Material.DISPENSER && action && (trigger || (main.utils.canBePowered(b) && main.utils.redstoneNextTo(b)) || main.utils.canCarryPower(b)) ){ 
            Player p = e.getPlayer();
            main.utils.checkedLocations=new ArrayList<>();  //needs to be manually reset                                                                                                                            
            boolean hasPermission = p.hasPermission("donordispense.dispense") || p.isOp();                                                                                              
            if(main.utils.isConnectedToDispenser(b) && !hasPermission){                                                                                                                 
                e.setCancelled(true);                                                                                                                                                   
                main.utils.sendNoPerms(p);                                                                                                                                              
            }    
        }        
    }  
    
    /*
    * Event handler for player block place events
    * @param BlockPlaceEvent e contains crucial information about the event.
    * If the event isnt cancelled, the block is a trigger/can be powered/can complete a circuit,
    *    and the block can either be able to: 
    *        1. be powered, and is next to a possible power source. 
    *        2. generate power. 
    *        3. carry power (e.g. wire)
    */
    @EventHandler(priority=EventPriority.HIGHEST)
    public void onBlockPlace(BlockPlaceEvent e){
        Block b= e.getBlock();
        boolean trigger =main.utils.isTrigger(b);
        if(!e.isCancelled() && (trigger || (main.utils.canBePowered(b) && main.utils.redstoneNextTo(b)) || main.utils.canCarryPower(b)) ){
            Player p = e.getPlayer();
            main.utils.checkedLocations=new ArrayList<>();
            boolean hasPermission = p.hasPermission("donordispense.dispense") || p.isOp();
            if(main.utils.isConnectedToDispenser(b) && !hasPermission){
                e.setCancelled(true);
                main.utils.sendNoPerms(p);
            }
        }
    }
    
    /*
    * Event handler for entity trigger events, such as cows stepping on pressure plates
    * @param EntityInteractEvent e contains crucial information about the event.
    * If the event isnt cancelled, entity is not player, and block can be triggered by entity (pressure plate vs button)
    */
    @EventHandler(priority=EventPriority.HIGHEST)
    public void onEntityTriggerInteract(EntityInteractEvent e){
        Block b = e.getBlock();
        if(!e.isCancelled() && e.getEntityType()!=EntityType.PLAYER && b!=null  && main.utils.isTriggerableByEntity(b))
            e.setCancelled(true);
    }
    
    /*
    * Event handler for piston extension events
    * @param BlockPistonExtendEvent e contains crucial information about the event.
    * If the event isnt cancelled, or any surrounding block in a 3x3x3 area
    */
    @EventHandler(priority=EventPriority.HIGHEST)
    public void onPistonExtend(BlockPistonExtendEvent e){
        ArrayList<Block> blocks = new ArrayList<>(e.getBlocks());
            for(Block b:blocks)
                if(main.utils.isConnectedToDispenser(b))
                    e.setCancelled(true);
    }
    
    /*
    * Event handler for piston retraction events
    * @param BlockPistonRetractEvent e contains crucial information about the event.
    * If the event isnt cancelled, or any surrounding block in a 3x3x3 area
    */
    @EventHandler(priority=EventPriority.HIGHEST)
    public void onPistonRetract(BlockPistonRetractEvent e){
        ArrayList<Block> blocks = new ArrayList<>(e.getBlocks());
            for(Block b:blocks)
                if(main.utils.isConnectedToDispenser(b))
                    e.setCancelled(true);
    }
    //@TODO redstone NEXT to block powered by tripwire (under works fine)(done?)
    @EventHandler(priority=EventPriority.HIGHEST)
    public void onBlockBreak(BlockBreakEvent e){
        Block b= e.getBlock();
        if(!e.isCancelled() && main.utils.isTrigger(b)){
            Player p = e.getPlayer();
            main.utils.checkedLocations=new ArrayList<>();
            boolean hasPermission = p.hasPermission("donordispense.dispense") || p.isOp();
            if(main.utils.isConnectedToDispenser(b) && !hasPermission){
                e.setCancelled(true);
                main.utils.sendNoPerms(p);
            }
        }
    }
}