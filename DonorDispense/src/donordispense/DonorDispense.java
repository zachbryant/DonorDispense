package donordispense;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.event.HandlerList;

public class DonorDispense extends JavaPlugin{
    protected DispenseListener listener;
    protected DonorDispenseUtils utils;

    @Override
    public void onEnable(){
        setEnabled(true);
        utils=new DonorDispenseUtils(this);
        listener=new DispenseListener(this);
        getServer().getPluginManager().registerEvents(listener,this);
        getCommand("dd").setExecutor(utils);    
    }
    
    @Override
    public void onDisable(){HandlerList.unregisterAll(this);}
    
}
