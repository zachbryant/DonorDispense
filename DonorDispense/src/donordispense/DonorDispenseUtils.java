package donordispense;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Dispenser;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class DonorDispenseUtils implements CommandExecutor{
    private final DonorDispense main;
    protected String version;
    ArrayList<Location> checkedLocations = new ArrayList<>();
    
    public DonorDispenseUtils(DonorDispense m){
        main=m; 
        version=m.getDescription().getVersion();
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
        if(cmd.getName().equalsIgnoreCase("dd")){
            displayInfo(sender);
            return true;
        }
        return false;
    }
    
    protected void displayInfo(CommandSender sender)
    {           
        sender.sendMessage(ChatColor.GRAY+"=-=-=-=-=-=-=-=-=-=-=-=-=-=["+ChatColor.DARK_AQUA+"DonorDispense"+ChatColor.GRAY+"]=-=-=-=-=-=-=-=-=-=-=-=-=-=");
        sender.sendMessage(ChatColor.GRAY+"Donor Dispense(v"+version+") was developed by burnt_apples for Dethemium.\nPlease fill out a ticket if you spot a bug.");
    }
    
    protected void sendNoPerms(Player p)
    {
        p.sendMessage(ChatColor.RED+"You are not allowed dispense destructive items.");
    }
    
    protected boolean isTrigger(Block b){
        if(b==null)
            return false;
        switch(b.getType()){
            case LEVER:                     
            case STONE_PLATE:               
            case WOOD_PLATE:                
            case GOLD_PLATE:                
            case IRON_PLATE:                
            case DIODE_BLOCK_ON:            
            case REDSTONE_BLOCK:            
            case REDSTONE_TORCH_OFF:        
            case REDSTONE_TORCH_ON:         
            case TRIPWIRE:                  
            case TRIPWIRE_HOOK:            
            case WOOD_BUTTON:               
            case STONE_BUTTON:             
            case DAYLIGHT_DETECTOR:        
            case DAYLIGHT_DETECTOR_INVERTED: return true;
            default:                        return false;
        }
    }
    
    protected boolean isTriggerableByEntity(Block b){
        if(b==null)
            return false;
        switch(b.getType()){
            case STONE_PLATE:               
            case WOOD_PLATE:                
            case GOLD_PLATE:                
            case IRON_PLATE:                
            case TRIPWIRE:                  
            case TRIPWIRE_HOOK: return true;
            default:            return false;
        }
    }
    
    protected boolean contains(Location l){
        for (Location check : checkedLocations)
            if(l.getWorld()==check.getWorld() && (check.getBlockX()==l.getBlockX())&&(check.getBlockY()==l.getBlockY())&&(check.getBlockZ()==l.getBlockZ()))
                return true;

        return false;
    }
    
    protected boolean containsBanned(Inventory i)throws NullPointerException{
        for(ItemStack is:i.getContents()){
            if(is==null)
                continue;
            switch(is.getType()){
                case FLINT_AND_STEEL:
                case MONSTER_EGG:    
                case EXP_BOTTLE:     
                case LAVA_BUCKET:    
                case FIREBALL:       return true;    
                //Defaulting allows the loops to be cut short, so no default case
            }
        }
        return false;
    }
    
    protected boolean isConnectedToDispenser(Block b){
        if(b==null)
            return false;
        if(b.getType()==Material.DISPENSER){
            Dispenser dispenser = (Dispenser)b.getState();
            Inventory inv = dispenser.getInventory();
            try{
                return containsBanned(inv);
            }catch(NullPointerException np){}
            return false;
        }
        
        HashSet<Block> adj = (b.getType()==Material.TRIPWIRE_HOOK || b.getType()==Material.TRIPWIRE?surroundingBlocks(b,null,4):surroundingBlocks(b,null,3));
        if(adj.isEmpty())
            return false;
        for(Block c:adj){
            Location tempLoc = c.getLocation();
            if(contains(tempLoc))
                return false;
            checkedLocations.add(tempLoc);
            //if not dispenser, recurse through possible paths and add to checked locations
            switch(c.getType()){
                case DISPENSER: Dispenser dispenser = (Dispenser)c.getState();
                                Inventory inv = dispenser.getInventory();
                                try{
                                    if(containsBanned(inv))
                                         return true;
                                }catch(NullPointerException np){}
                                return false;
                case DIODE_BLOCK_OFF:
                case DIODE_BLOCK_ON:
                case REDSTONE_COMPARATOR:
                case REDSTONE_COMPARATOR_ON:
                case REDSTONE_COMPARATOR_OFF:
                case REDSTONE_TORCH_OFF:
                case REDSTONE_TORCH_ON:
                case TRIPWIRE:
                case TRIPWIRE_HOOK:
                case REDSTONE_WIRE: return isConnectedToDispenser(c);
                case IRON_BLOCK:        
                case HAY_BLOCK:         
                case NOTE_BLOCK:        
                case OBSIDIAN:          
                case REDSTONE_BLOCK:    
                case DIRT:              
                case GRASS:             
                case GRAVEL:            
                case SAND:              
                case BOOKSHELF:         
                case BRICK:             
                case COBBLESTONE:       
                case DIAMOND_BLOCK:     
                case DOUBLE_STEP:       
                case DOUBLE_STONE_SLAB2:
                case EMERALD_BLOCK:     
                case GOLD_BLOCK:        
                case LAPIS_BLOCK:       
                case LOG:               
                case LOG_2:             
                case MELON_BLOCK:       
                case MOSSY_COBBLESTONE: 
                case MYCEL:             
                case NETHER_BRICK:      
                case NETHERRACK:        
                case PRISMARINE:        
                case PUMPKIN:           
                case QUARTZ_BLOCK:      
                case SANDSTONE:         
                case SEA_LANTERN:       
                case SLIME_BLOCK:       
                case SMOOTH_BRICK:      
                case SMOOTH_STAIRS:     
                case SOIL:              
                case SOUL_SAND:         
                case SPONGE:
                case STAINED_CLAY:
                case STEP:
                case STONE_SLAB2:
                case TRAPPED_CHEST:
                case WOOD:
                case WOOD_STEP:
                case WOOL:
                case COAL_ORE:
                case DIAMOND_ORE:
                case REDSTONE_ORE:
                case GOLD_ORE:
                case IRON_ORE:
                case LAPIS_ORE:
                case EMERALD_ORE:
                case GLOWING_REDSTONE_ORE:
                case QUARTZ_ORE:
                case STONE: HashSet<Block> dispensers = surroundingBlocks(c,Material.DISPENSER,3);
                            for(Block d:dispensers)
                                if(isConnectedToDispenser(d))
                                    return true;
                            ArrayList<Block> extenders = currentExtenders(c);
                            for(Block d:extenders)
                                if(isConnectedToDispenser(d))
                                    return true;
                default: break;
            }
        }
        return false;
    }
    
    protected boolean redstoneNextTo(Block b){
        HashSet<Block> list = surroundingBlocks(b,null,3);
        for(Block c:list)
            if(canCarryPower(c))
                return true;
        return false;
    }
    
    protected HashSet<Block> surroundingBlocks(Block b, Material search, int length){
        HashSet<Block> list = new HashSet<>();
        int redstoneCount=0;
        for(int c=0-(length/2);c<length-1;c++){ //grab a length x length x length cube around (not including) the location
            for(int d=0-(length/2);d<length-1;d++){
                for(int e=-1;e<2;e++){
                    if(c==0 && d==0 && e==0)
                        continue;
                    Block temp =b.getRelative(c, e, d);
                    //Skip over materials not matching search parameters
                    if(search !=null)
                        if(search == temp.getType()){
                            list.add(temp);
                            redstoneCount++;
                        }
                        else
                            continue;
                    boolean checked = contains(temp.getLocation());
                    //only send blocks that can power, not other irrelevant blocks
                    if(canCarryPower(temp) && !checked){
                        redstoneCount++;
                        list.add(temp);
                        continue;
                    }
                    if(canBePowered(temp) && !checked){
                        list.add(temp);
                        if(currentExtenders(temp).size()>0)
                            redstoneCount++;
                        continue;
                    }
                    if(temp.getType()==Material.DISPENSER){
                        list.add(temp);
                        redstoneCount++;
                    }
                }
            }
        }
        if(redstoneCount>0)
            return list;
        return new HashSet<>();
    }
    
    protected boolean canCarryPower(Block b){
        if(b==null)
            return false;
        switch(b.getType()){
            case DISPENSER:              
            case REDSTONE_WIRE:          
            case DIODE_BLOCK_ON:         
            case DIODE_BLOCK_OFF:         
            case TRIPWIRE:               
            case REDSTONE_BLOCK:         
            case REDSTONE_COMPARATOR:     
            case REDSTONE_COMPARATOR_OFF: 
            case REDSTONE_COMPARATOR_ON: 
            case REDSTONE_TORCH_OFF:     
            case REDSTONE_TORCH_ON:       return true;
            default:                      return false;
        }
    }

    protected ArrayList<Block> currentExtenders(Block b){
        ArrayList<Block> list = new ArrayList<>();
        if(b==null)
            return list;
        //Is redstone on top of this block?
        //Is this block connected to any diodes, comparators, or redstone torches?
        Block[] dirs = new Block[]{ b.getRelative(BlockFace.UP),
                                    b.getRelative(BlockFace.SOUTH),
                                    b.getRelative(BlockFace.DOWN),
                                    b.getRelative(BlockFace.EAST),
                                    b.getRelative(BlockFace.WEST),
                                    b.getRelative(BlockFace.NORTH) };
        
        for(Block c:dirs)
            if(canCarryPower(c) && !contains(c.getLocation()))
                list.add(c);

        return list;
    }
    protected boolean canBePowered(Block b){
        if(b==null)
            return false;
        switch(b.getType()){
            case IRON_BLOCK:       
            case HAY_BLOCK:          
            case NOTE_BLOCK:         
            case OBSIDIAN:           
            case DIRT:               
            case GRASS:              
            case GRAVEL:             
            case SAND:               
            case BOOKSHELF:          
            case BRICK:              
            case COBBLESTONE:        
            case DIAMOND_BLOCK:      
            case DOUBLE_STEP:        
            case DOUBLE_STONE_SLAB2: 
            case EMERALD_BLOCK:      
            case GOLD_BLOCK:         
            case LAPIS_BLOCK:        
            case LOG:                
            case LOG_2:              
            case MELON_BLOCK:        
            case MOSSY_COBBLESTONE:  
            case MYCEL:              
            case NETHER_BRICK:       
            case NETHERRACK:         
            case PRISMARINE:         
            case PUMPKIN:            
            case QUARTZ_BLOCK:       
            case SANDSTONE:          
            case SEA_LANTERN:        
            case SLIME_BLOCK:        
            case SMOOTH_BRICK:
            case SMOOTH_STAIRS:      
            case SOIL:               
            case SOUL_SAND:          
            case SPONGE:             
            case STAINED_CLAY:       
            case STEP:               
            case STONE:              
            case STONE_SLAB2:        
            case TRAPPED_CHEST:      
            case WOOD:               
            case WOOD_STEP:          
            case WOOL:               
            case COAL_ORE:           
            case DIAMOND_ORE:        
            case REDSTONE_ORE:       
            case GOLD_ORE:           
            case IRON_ORE:           
            case LAPIS_ORE:          
            case EMERALD_ORE:        
            case GLOWING_REDSTONE_ORE: 
            case QUARTZ_ORE:        return true;
            default:                return false;
        }
    }
}
