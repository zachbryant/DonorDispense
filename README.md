**Overview**

DonorDispense uses recursion to track the pathway from several power-generating events such as:

piston extension/retention

block breaking/placing (tripwires,power generating blocks)

entity/player interaction


**Permissions**

donordispense.dispense or OP powers will suffice.

**All redstone triggers**

LEVER, STONE_PLATE, WOOD_PLATE, GOLD_PLATE, IRON_PLATE, DIODE_BLOCK_ON, 

REDSTONE_BLOCK, REDSTONE_TORCH_OFF, REDSTONE_TORCH_ON, TRIPWIRE, 

TRIPWIRE_HOOK, WOOD_BUTTON, STONE_BUTTON, DAYLIGHT_DETECTOR, 

DAYLIGHT_DETECTOR_INVERTED

**All Dispensible Destructive Items**

FLINT_AND_STEEL, MONSTER_EGG, EXP_BOTTLE, LAVA_BUCKET, FIREBALL

**Planned Features**

Configurable banned dispensable items

Configurable levels of dispensible items